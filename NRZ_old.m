Fs = 32;     %sample rate (kHz)
Rs = 1.6;    %symbol rate (kHz)
Ns = Fs/Rs;  %number of samples per symbol
Nb = 30;     %number of bits to simulate

%---Discrete B(z)/A(z) model of telephone channel---
A = [1.00, -2.838, 3.143, -1.709, 0.458, -0.049];
B = 0.1*[1,-1];

%---Pulse shape the data ----
pulse = [ones(1,Ns)];    %polar NRZ pulse
data = sign(randn(1,Nb));

Sig = pulse' * data;
Sig = Sig(:);

%---Pass signal through the channel ----
RxSig = filter(B,A,Sig);

%---Plot results------
plot(real(RxSig))
hold on, plot(Sig,'r'), hold off
xlabel('Time samples'), ylabel('Amplitude')

