close all %TODO remove
% --- INPUTS ---
Nb = 30;     %number of bits to simulate

toCompare = { ... %Encoding scheme, symbol rate (Rs), sample rate (Fs)
    'NRZ',        0.8, 32; ...
    'Manchester', 1.6, 32; ...
    'Manchester', 6.4, 256; ...
    };

%---Discrete B(z)/A(z) model of telephone channel--- 
A = [1.00, -2.838, 3.143, -1.709, 0.458, -0.049];
B = 0.1*[1,-1];

% ---PROCESSING ---

%Generate the random bits to be used. Generated as polar NRZ
data = sign(randn(1,Nb));

signals = cell2struct(toCompare,{'Scheme','Rs','Fs'},2);

%Adding in necessary fields
for i = 1:length(signals)
    signals(i).Ns    = signals(i).Fs/signals(i).Rs; %Number of samples per signal
    
    if strcmpi(signals(i).Scheme,'NRZ')
        signals(i).pulse = ones(1,signals(i).Ns);       %Pulse shape the data
        signals(i).data = data;
        
    elseif strcmpi(signals(i).Scheme,'Manchester')
        signals(i).pulse = ones(1,signals(i).Ns/2);       %Pulse shape the data
        signals(i).data = zeros(1,2*length(data));
        
        if data(1) == 1 %Just to set the first bit to base the rest off
            signals(i).data(1:2) = [1, -1];
        else
            signals(i).data(1:2) = [-1, 1];
        end
        
        for k = 2:length(data)
            j=2*k;
            if data(k) == 1 %If 1
                signals(i).data(j-1:j) = -signals(i).data(j-3:j-2); %Copies in the same bit as previous
            else
                signals(i).data(j-1:j) = signals(i).data(j-3:j-2); %Inverts previous bit instead
            end
        end
        
    else
        error('Field %i has scheme %s when it should be NRZ or Manchester',i,signals(i).Scheme)
    end
    
    signals(i).Sig = signals(i).pulse' * signals(i).data;
    signals(i).Sig = signals(i).Sig(:);
    
    %Pass signal through channel
    signals(i).RxSig = filter(B,A,signals(i).Sig);
    
end

%Finding correlations for all signals
for i=1:length(signals)
    signals(i).XCorr = xcorr(signals(i).Sig,signals(i).RxSig,0,'coeff');
end
%Construct Filters
[ coeffsS ] = getMatchedFilter( signals(2).Ns );
%Apply Filters
%To apply MF to chanel output****************
sS=signals(2).RxSig*coeffsS;
%Adding Noise to the Signal
RxSigwithNoise = awgn(sS,0);
hmatch = transpose(coeffsS);
x = conv(hmatch, RxSigwithNoise(:,end));
figure(2);
ploteye(x,signals(2).Ns) %plots eye digram of matched filter output
title('Eye Diagram'),xlabel('Time samples'), ylabel('Amplitude'),
