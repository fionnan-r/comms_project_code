%Eye Diagrams are produced for both differential manchester encoded signals
%(0.8kbit/s and 6.4kbit/s)

%******************************************************

%Requires the following before running :
%   -transmit.m and relevant files
%   -getMatchedFilter function
%   -ploteye function


%Construct Filters
[ coeffsS ] = getMatchedFilter( signals(2).Ns );
[ coeffsF ] = getMatchedFilter( signals(3).Ns );

%Apply Filters
%To apply MF to chanel output****************
sS=signals(2).RxSig*coeffsS;
sF=signals(3).RxSig*coeffsF;

%plot Eye Diagrams
figure(2);
ploteye(sS,signals(2).Ns);
figure(3);
ploteye(sF,signals(3).Ns);


