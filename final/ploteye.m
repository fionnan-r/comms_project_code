function b = ploteye(s,Ns);
%-------------------------------------
%Inputs
%  s - real signal
%  Ns - oversample rate
%-------------------------------------
f = mod(length(s),Ns);
s = real(s(1:end-f));

%---extract individual symbol periods from signal---
EyeSigRef = reshape(s,Ns,length(s)/Ns); %one symbol per column
EyeSigm1  = EyeSigRef(Ns/2+1:Ns,1:end-2); %last half of preceding symbol
EyeSigO   = EyeSigRef(: ,2:end-1);      %current symbol
EyeSigp1  = EyeSigRef(1:Ns/2, 3:end);   %first half of following symbol
EyeSig    = [EyeSigm1; EyeSigO; EyeSigp1]; %track together for curve

L = size(EyeSig,1);
plot([0:L-1]/Ns,EyeSig)      %plot multiple curves