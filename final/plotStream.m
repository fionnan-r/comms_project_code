function plotStream(signal, Nb, Ns)
% function plotStream(signal, Nb, Ns)
% Displays a digital signal with shading to indicate bits
%
% signal    Matrix or cell array of matricies which contain the values for
%           the data stream
% Nb        Number of bits simulated in the stream
% Ns        Number of samples per signal

if isnumeric(signal)
    plot(signal)
    setAxis(Nb,Ns);
    
elseif iscell(signal) %Just iterates through above if it's a cell
    numberSignals = length(signal);
    for i=1:numberSignals
        subplot(numberSignals,1,i)
        plot(signal{i})
        setAxis(Nb,Ns);
    end
else
    warning('Couldn''t plot. Signal is class %s when it should be an array or cell of arrays',class(signal))
    return
end
set(gcf,'color','white') %Set figure colour white

end

function setAxis(Nb,Ns)
% Sets the axis up, including doing things like shading
grid on
axis = gca;
%axis.YLim = axis.YLim*1.5; %Pushes out the axis limits slightly
axis.YMinorTick = 'on';
axis.XTick = (1:Nb)*Ns;
axis.YTick = [-1,0,1];
axis.XLim  = [0 Nb*Ns];
%axis.XTickLabel = '';
xlabel('Time samples'), ylabel('Amplitude')

gray = [0.4 0.4 0.4];
for i=1:2:Nb %Adds in alternating shaded areas for each bit
    xVal1 = i*Ns;
    xVal2 = (i+1)*Ns;
    yVal = 2;
    
    shade = patch([xVal1,xVal1,xVal2,xVal2],[yVal,-yVal,-yVal,yVal],gray); %Adds in patches - grey rectangles to indicate bits
    shade.FaceAlpha = 0.1; %Set the transperancy of the patches low
    shade.LineStyle = 'none'; %Don't need more grid lines
end
end