%Run by using 'Run Section' or else individually execute each code block

%Setup some variables
filterCoeffs.A = [1.00, -2.838, 3.143, -1.709, 0.458, -0.049];
filterCoeffs.B = 0.1*[1,-1];
Nb = 30;

%% Question 1.a
toCompare_q1a = { ... %Encoding scheme, symbol rate (Rs), sample rate (Fs)
    'NRZ',        0.8, 32; ...
    'Manchester', 0.8, 32; ...
    };
signals_q1a = transmit(toCompare_q1a,filterCoeffs,Nb);

%% Question 1.b
toCompare_q1b = { ... %Encoding scheme, symbol rate (Rs), sample rate (Fs)
    'Manchester', 0.8, 32; ...
    'Manchester', 6.4, 32; ...
    'Manchester', 6.4, 256; ...
    };
signals_q1b = transmit(toCompare_q1b,filterCoeffs,Nb);

%% Question 1.c
%Apply matched filter to the struct from q1b. Only converned about the 2nd and 3rd elements though
signals_q1b = applyMatchedFilter(signals_q1b);

%Plot the resulting eye diagrams
subplotEyes(signals_q1b(1:2));
%% Question 1.d
toCompare_q1d = {'Manchester', 1.6, 32};

signals_q1d = transmit(toCompare_q1d,filterCoeffs,Nb);  %Generate the necessary signals eg. Tx, Rx etc
signals_q1d = applyMatchedFilter(signals_q1d);          %Apply a generated matched filter to the signal

signals_q1d.RxSigwithNoise = awgn(signals_q1d.matchedFilt,0); %Add noise to the received signal
signals_q1d.hmatch = transpose(signals_q1d.MFCoeffs);

signals_q1d.matchedFilt = conv(signals_q1d.hmatch, signals_q1d.RxSigwithNoise(:,end));
subplotEyes(signals_q1d);