function signals = transmit(toCompare,filterCoeffs,Nb)
% function signals = transmit(toCompare,filterCoeffs,Nb)
% 
% signals       Struct that's returned with fields containing:
%               Scheme name, Rs, Fs, Ns, transmitted signal, received
%               signal, cross-correlation between Tx and Rx
% toCompare     %Encoding scheme, symbol rate (Rs), sample rate (Fs)
% filterCoeffs  Struct of the channel filter coefficients with
%               filterCoeffs.A and filterCoeffs.B included
% Nb            Number of bits to simulate
%
% Example:
%       filterCoeffs.A = [1.00, -2.838, 3.143, -1.709, 0.458, -0.049];
%       filterCoeffs.B = 0.1*[1,-1];
%       Nb = 30;
%       toCompare = {
%           'NRZ',        0.8, 32; ...
%           'Manchester', 0.8, 32 ...
%       };
%       signals = transmit(toCompare,filterCoeffs,Nb);


%Takes the filter coeffs out of the struct
A = filterCoeffs.A;
B = filterCoeffs.B;

%Generate the random bits to be used. Generated as polar NRZ
data = sign(randn(1,Nb));

signals = cell2struct(toCompare,{'Scheme','Rs','Fs'},2);

%Adding in necessary fields
for i = 1:length(signals)
    signals(i).Ns = signals(i).Fs/signals(i).Rs; %Number of samples per signal
    
    if strcmpi(signals(i).Scheme,'NRZ')
        signals(i).pulse = ones(1,signals(i).Ns);       %Pulse shape the data
        signals(i).data = data;
        
    elseif strcmpi(signals(i).Scheme,'Manchester')
        signals(i).pulse = ones(1,signals(i).Ns);       %Pulse shape the data
        signals(i).data = zeros(1,2*length(data));
        
        if data(1) == 1 %Just to set the first bit to base the rest off
            signals(i).data(1:2) = [1, -1];
        else
            signals(i).data(1:2) = [-1, 1];
        end
        
        for k = 2:length(data)
            j=2*k;
            if data(k) == 1 %If 1
                signals(i).data(j-1:j) = -signals(i).data(j-3:j-2); %Copies in the same bit as previous
            else
                signals(i).data(j-1:j) = signals(i).data(j-3:j-2); %Inverts previous bit instead
            end
        end
        signals(i).data = signals(i).data(1:end); %TODO to fix Fs
        signals(i).Ns = signals(i).Ns*2;
    else
        error('Field %i has scheme ''%s'' when it should be ''NRZ'' or ''Manchester''',i,signals(i).Scheme)
    end
    
    signals(i).Sig = signals(i).pulse' * signals(i).data;
    signals(i).Sig = signals(i).Sig(:);
    
    %Pass signal through channel
    signals(i).RxSig = filter(B,A,signals(i).Sig);
    
end

%Plotting results
figure;
for i = 1:length(signals)
    subplot(length(signals),1,i)
    hold on
    plotStream(signals(i).Sig,Nb,(signals(i).Ns)); %Plot the Tx signal
    plot(real(signals(i).RxSig)) %Plot the Rx signal
    title(sprintf('(%s) %s: Rs=%g, Fs=%g',char(i+96),signals(i).Scheme,signals(i).Rs,signals(i).Fs)); %Give a name in the format eg (a) NRZ: Rs= ,Fs=
    hold off
end

%Finding correlations for all signals
for i=1:length(signals)
    signals(i).XCorr = xcorr(signals(i).Sig,signals(i).RxSig,0,'coeff');
end
end
