function subplotEyes(signals)
% function subplotEyes(signals)
% Takes a signals struct and makes eyeplots of the matched filter outputs
% Inputs:
%   signals Signals struct

fig = figure;
for i=1:length(signals) %Goes through and makes an individual subplot for each eye
    subplot(length(signals),1,i);
    ploteye(signals(i).matchedFilt,signals(i).Ns); %Uses given ploteye function
    title(sprintf('%s with Rs=%g and Fs = %g',signals(i).Scheme,signals(i).Rs,signals(i).Fs));
    xlabel('Time samples'), ylabel('Amplitude')
end
fig.Color = 'white'; %Make it pretty
end