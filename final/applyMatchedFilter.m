function signals = applyMatchedFilter (signals)
% function signals = applyMatchedFilter (signals)
% 
% Takes a signals struct and generates the matched filter coefficients for
% it. Then applies them to generate a filtered output. This is then
% returned.

for i = 1:length(signals)
    signals(i).MFCoeffs = getMatchedFilter( signals(i).Ns ); %Get the coefficients required for the matched filter
    signals(i).matchedFilt=signals(i).RxSig*signals(i).MFCoeffs; %Apply matched filter to channel output
end

end

function [ coeffs ] = getMatchedFilter( Ns )
% Generates matched filter coefficients for
% Differential Manchester

% Input: Ns is number of samples per symbol
% (must be an even number)
DM_Pulse_Shape = [ones(1,Ns/2),-1*ones(1,Ns/2)];
coeffs = fliplr(DM_Pulse_Shape);
end

