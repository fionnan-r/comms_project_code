%PAI = 3.1416;

Fs = 32;     %samples per BPSK symbol

%Rb = 10000;  %Data rate in bits/s

%SNR = -10;
EbN0 = 2; %in dB

%EbN0 = 10^(EbN0dB/10);

%Eb = 1/Rb;

SNR = EbN0 - 10*log10(Fs/2);

%t = [1:1:2*Fs-1]/Rb/Fs;
biterrors = 0;

for i = 1:1:10000
    
bit = (randn > 0);

data = (2*bit - 1) * ones(Fs,1);

hm = ones(Fs,1); %matched filter

sig = awgn(data, SNR);

c = conv(sig,hm);

if (bit ~= (c(Fs)>0)) 
    biterrors = biterrors + 1;
end

end

ber = biterrors/10000

%Ns = Fs/Rs;  %number of samples per symbol
%Nb = 30;     %number of bits to simulate

%---Discrete B(z)/A(z) model of telephone channel---
%A = [1.00, -2.838, 3.143, -1.709, 0.458, -0.049];
%B = 0.1*[1,-1];

%---Pulse shape the data ----
%pulse = [ones(1,Ns)];    %bipolar NRZ pulse
%data = sign(randn(1,Nb));

%Sig = pulse' * data;
%Sig = Sig(:);

%---Pass signal through the channel ----
%RxSig = filter(B,A,Sig);

%---Plot results------
%plot(real(RxSig))
%hold on, plot(Sig,'r'), hold off
%xlabel('Time samples'), ylabel('Amplitude')

