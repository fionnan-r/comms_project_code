Fs = 32;     %sample rate (kHz)
Rs = 0.8;    %symbol rate (kHz)
Ns = Fs/Rs;  %number of samples per symbol
Nb = 30;     %number of bits to simulate

%---Discrete B(z)/A(z) model of telephone channel---
A = [1.00, -2.838, 3.143, -1.709, 0.458, -0.049];
B = 0.1*[1,-1];

%---Pulse shape the data ----
data = sign(randn(1,Nb));    %polar NRZ pulse

%Using the textbook data
data = [1,-1,1,-1,1,1,1,-1,-1];
Nb = 9;

if mod(Ns,2) == 1
pulse = [ones(1,2*Ns)];
x = [0.5:0.5:Ns*Nb]';
elseif mod(Ns,2) == 0
    pulse = [ones(1,Ns)];
    x = [1:1:Ns*Nb]';
end

for ii = 1:1:size(data,2)
    if data(ii) == -1
        data(ii) = 0;
    end
end

Sig = pulse' * data;
Sig = Sig(:);
Sig1 = Sig;

for jj = 1:1:Nb
    if jj == 1
        if data(jj) == 1
            for kk = Ns/2:1:Ns
                Sig1(kk) = 0;
            end
        elseif data(jj) == 0
            for kk = Ns/2:1:Ns
                Sig1(kk) = 1;
            end
        end
    end
    
    if jj ~= 1
        if data(jj) == 0
            for kk = 1:1:Ns
                Sig1((jj-1)*Ns+kk) = Sig1((jj-2)*Ns+kk);
            end
        elseif data(jj) == 1
            for kk = 1:1:Ns
                Sig1((jj-1)*Ns+kk) = abs(1-Sig1((jj-2)*Ns+kk));
            end
        end
    end
end

%---Pass signal through the channel ----
RxSig = filter(B,A,Sig1);

%---Plot results------
figure
plot(x, real(RxSig))
hold on, plot(x, Sig1,'r'), hold off
xlabel('Time samples'), ylabel('Amplitude') 

figure
plot(Sig1,'b')
hold on, plot(Sig, 'r'), hold off
axis([0 1200 -2 2])
