function BER_NRZ()
tic;
Fs = 32;     %samples per BPSK symbol
biterrors = 0;

% Set-Up EbN0 limits
N_max = 10;
N_min = -2;
Number_Data_Points = N_max-N_min+1;
    
ber_theoretical_vector= ones(Number_Data_Points,1);

for ii = N_min:1:N_max
    
    ber_theoretical = qfunc(sqrt(2*10^(ii/10)));
    ber_theoretical_vector(ii-N_min+1) = ber_theoretical;

end

ber_vector = ones(Number_Data_Points,3);
EbN0_vector = [N_min:1:N_max];

for j = 0:1:2

for k = N_min:1:N_max
    
    biterrors = 0;
    
    EbN0 = k;
    SNR = EbN0 - 10*log10(Fs/2);
    
for i = 1:1:10000*10^(j)
    
bit = (randn > 0);

data = (2*bit - 1) * ones(Fs,1);

hm = ones(Fs,1); %matched filter

sig = awgn(data, SNR);

c = conv(sig,hm);

if (bit ~= (c(Fs)>0)) 
    biterrors = biterrors + 1;
end

end

ber = biterrors/(10000*10^(j));
ber_vector(k-N_min+1,j+1) = ber;

end

end

figure
semilogy(EbN0_vector,ber_vector(:,1),'o');
hold on
semilogy(EbN0_vector,ber_vector(:,2),'+')
semilogy(EbN0_vector,ber_vector(:,3),'*')
semilogy(EbN0_vector,ber_theoretical_vector,'x');
xlabel('E_b / N_0'), ylabel('BER (log scale)');
legend('1000 bits','10000 bits','1000000 bits','Theoretical BER');
title('BER vs E_b / N_0 for BPSK Signal');
hold off

figure
semilogy(EbN0_vector,ber_vector(:,1),'r');
hold on
semilogy(EbN0_vector,ber_vector(:,2),'g')
semilogy(EbN0_vector,ber_vector(:,3),'b')
semilogy(EbN0_vector,ber_theoretical_vector,'y');
xlabel('E_b / N_0'), ylabel('BER (log scale)')
legend('1000 bits','10000 bits','1000000 bits','Theoretical BER');
title('BER vs E_b / N_0 for BPSK Signal');
hold off

timeElapsed = toc

end
